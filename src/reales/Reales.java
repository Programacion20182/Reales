package reales;

// Llamamos a la clase Scanner que se encuentra en el paquete java.util
import java.util.Scanner;

/**
 * Clase de numeros reales (Flotantes).
 * @author diego
 */
public class Reales {
    
    // Variable de la clase Reales (Flotantes).
    float f;
    
    /**
     * Metodo que nos permite crear un objeto del tipo Reales.
     * @param f Valor del flotante.
     */
    Reales(float y){
        // Guardamos el valor del parametro en la variable de la clase.
        this.f = y;
    }
    
    /**
     * Metodo encargado de calcular el valor absoluto.
     * Este metodo se llamara CON la instancia de un objeto.
     * @return El valor absoluto.
     */
    float valorAbsoluto(){
        // Variable donde se guardara el valor absoluto de x.
        float resultado;
        // Si n es positivo entonces regresamos n.
        if(this.f >= 0)
            resultado = this.f;
        // Si es negativo, entonces lo pasamos a positivo.
        else
            resultado = -this.f;
        // Regresamos  el resultado.
        return resultado;
    }
    
    /**
     * Metodo encargado de calcular el valor absoluto de un numero.
     * Este metodo es estatico, por lo que puede se puede usar SIN la
     * instancia de un objeto.
     * @param n El numero al que le calcularemos el valor absoluto.
     * @return El valor absoluto de n.
     */
    static float valorAbsoluto(float x){
        // Variable donde se guardara el valor absoluto de x.
        float resultado;
        // Si n es positivo entonces regresamos n.
        if(x >= 0)
            resultado = x;
        // Si es negativo, entonces lo pasamos a positivo.
        else
            resultado = x * -1;
        // Regresamos  el resultado.
        return resultado;
    }
    
    /**
     * Metodo encargado de calcular la raiz cuadrada de un numero.
     * @param x El numero al que le sacaremos su raiz cuadrada.
     * @return La raiz cuadrada de x.
     */
    static float raizCuadrada(float x){
       // Declaramos las variables con las que trabajaremos.
       float b = x, h = 1, Ee = 0.00001f;
       // Se realizara este proceso hasta llegar a un valor menor al error esperado.
       for(;valorAbsoluto(b - h) > Ee;){
           b = (b + h) / 2;
           h = x / b;
       }
       return h;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Declaramos una variable flotante con identificador a.
        float a;
        
        // Creamos un objeto del tipo Reales.
        Reales r = new Reales(-25);
        
        // Imprimimos el valor guardado en el objeto r.
        System.out.println("El valor guardado en r es: " + r.f);
        
        /*
        System.out.println("El valor absoluto de " + a + " es: " + valorAbsoluto(a));
        */

        // Creamos el objeto que usaremos para tomar valores de consola.
        Scanner valores = new Scanner(System.in);
        
        System.out.println("Da un numero:");
        
        // nextFloat() va a aceptar valores flotantes que se guardaran en la variable a.
        a = valores.nextFloat();
        System.out.println("La raiz cuadrada de " + a + " es: " + raizCuadrada(a));
    }
}
